class URLConstant {
  static String baseURL = 'https://restaurant-api.dicoding.dev';
  static String smallImageUrl = '$baseURL/images/small/<pictureId>';
  static String mediumImageUrl = '$baseURL/images/medium/<pictureId>';
  static String largeImageUrl = '$baseURL/images/large/<pictureId>';

  static String pathListRestaurant = '/list';
}
