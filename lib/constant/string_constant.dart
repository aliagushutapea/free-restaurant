class StringConstant {
  static const appName = 'Free Restaurant';
  static const versionApp = 'v1.0.0';

  static const String favorites = 'Favorites';
}
