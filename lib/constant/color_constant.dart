import 'package:flutter/material.dart';

class ColorConstant {
  static const primaryColor = Color.fromRGBO(76, 175, 80, 1);
  static const lightPrimaryColor = Color.fromRGBO(200, 230, 201, 1);
  static const darkPrimaryColor = Color.fromRGBO(56, 142, 42, 1);

  static const accentColor = Color.fromRGBO(139, 195, 74, 1);
  static const primaryTextColor = Color.fromRGBO(33, 33, 33, 1);
  static const secondaryTextColor = Color.fromRGBO(117, 117, 117, 1);
  static const dividerColor = Color.fromRGBO(189, 189, 189, 1);

  static const whiteColor = Color.fromRGBO(255, 255, 255, 1);
  static const blackColor = Colors.black;
}
