abstract class Failure implements Exception {
  const Failure();
}

class OnInternalError extends Failure {
  const OnInternalError({this.caused = ''});

  final String caused;
}

class OnNotFound extends Failure {
  const OnNotFound({this.caused = ''});

  final String caused;
}

class OnDioException extends Failure {
  const OnDioException();

  
}
