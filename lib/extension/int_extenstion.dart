extension MapHTTPCode on int {
  bool badRequest() => this == 400;
  bool unauthorized() => this == 401;
  bool forbidden() => this == 403;
  bool notFound() => this == 404;
  bool created() => this == 201;
  bool success() => this == 200;
  bool pending() => this == 202;
  bool toManyAttempt() => this == 429;
  bool unprosessable() => this == 422;
  bool internalError() => this == 500;
  bool conflict() => this == 409;
  bool timeout() => this == 504;
}