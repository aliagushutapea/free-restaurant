import 'package:dio/dio.dart';
import 'package:free_restaurant/constant/url_constant.dart';
import 'package:free_restaurant/data/repository/restaurant_repository.dart';
import 'package:free_restaurant/feature/bloc/restaurant_bloc.dart';
import 'package:get_it/get_it.dart';

final sl = GetIt.instance;

Future<void> init() async {
  //factory

  sl.registerFactory<RestaurantBloc>(
    () => RestaurantBloc(
      restaurantRepository: sl(),
    ),
  );

  //singleton
  sl.registerLazySingleton<Dio>(() {
    final dio = Dio(
      BaseOptions(baseUrl: URLConstant.baseURL),
    );
    dio.options.receiveTimeout = const Duration(minutes: 1);
    dio.options.connectTimeout = const Duration(minutes: 1);
    dio.options.sendTimeout = const Duration(minutes: 1);
    return dio;
  });

  sl.registerLazySingleton(
    () => RestaurantRepository(
      dioInterceptor: sl(),
    ),
  );
}
