part of 'restaurant_bloc.dart';

@immutable
sealed class RestaurantState extends Equatable {
  const RestaurantState();

  @override
  List<Object?> get props => [];
}

final class RestaurantInitial extends RestaurantState {
  const RestaurantInitial();
}

final class AllRestaurantObtained extends RestaurantState {
  const AllRestaurantObtained({required this.listRestaurant});

  final List<Restaurant> listRestaurant;
}

final class RestaurantInternalError extends RestaurantState {
  const RestaurantInternalError();
}

final class RestaurantNotFound extends RestaurantState {
  const RestaurantNotFound();
}

final class RestaurantLoading extends RestaurantState {
  const RestaurantLoading();
}
