part of 'restaurant_bloc.dart';

@immutable
sealed class RestaurantEvent {
  const RestaurantEvent();
}

class GetListRestaurant extends RestaurantEvent{
  const GetListRestaurant();
}
