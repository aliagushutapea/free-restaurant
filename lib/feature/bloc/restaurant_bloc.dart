import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:free_restaurant/data/model/restaurant.dart';
import 'package:free_restaurant/data/repository/restaurant_repository.dart';
import 'package:free_restaurant/extension/int_extenstion.dart';
import 'package:meta/meta.dart';

part 'restaurant_event.dart';
part 'restaurant_state.dart';

class RestaurantBloc extends Bloc<RestaurantEvent, RestaurantState> {
  final RestaurantRepository restaurantRepository;
  RestaurantBloc({
    required this.restaurantRepository,
  }) : super(const RestaurantInitial()) {
    on<GetListRestaurant>((event, emit) async {
      try {
        emit(const RestaurantLoading());
        var listRestaurant = await restaurantRepository.getAllRestaurant();
        emit(AllRestaurantObtained(listRestaurant: listRestaurant));
      } on DioException catch (error) {
        if (error.response != null && error.response!.statusCode != null) {
          var statusCode = error.response!.statusCode!;
          if (statusCode.notFound()) {
            emit(const RestaurantNotFound());
          }
        }
      } catch (e) {
        emit(const RestaurantInternalError());
      }
    });
  }
}
