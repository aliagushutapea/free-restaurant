import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:free_restaurant/constant/color_constant.dart';
import 'package:free_restaurant/constant/string_constant.dart';
import 'package:free_restaurant/constant/url_constant.dart';
import 'package:free_restaurant/feature/bloc/restaurant_bloc.dart';
import 'package:free_restaurant/injection.dart';
import 'package:free_restaurant/data/model/restaurant.dart';
import 'package:free_restaurant/widgets/text/bold_text.dart';
import 'package:free_restaurant/widgets/text/medium_text.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final restaurantBloc = sl<RestaurantBloc>();
  List<Restaurant> listRestaurant = [
    Restaurant.fromMap({
      "id": "rqdv5juczeskfw1e867",
      "name": "Melting Pot",
      "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ...",
      "pictureId": "14",
      "city": "Medan",
      "rating": 4.2
    }),
    Restaurant.fromMap({
      "id": "rqdv5juczeskfw1e867",
      "name": "Melting Pot",
      "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ...",
      "pictureId": "14",
      "city": "Medan",
      "rating": 4.2
    }),
    Restaurant.fromMap({
      "id": "rqdv5juczeskfw1e867",
      "name": "Melting Pot",
      "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ...",
      "pictureId": "14",
      "city": "Medan",
      "rating": 4.2
    }),
    Restaurant.fromMap({
      "id": "rqdv5juczeskfw1e867",
      "name": "Melting Pot",
      "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ...",
      "pictureId": "14",
      "city": "Medan",
      "rating": 4.2
    }),
    Restaurant.fromMap({
      "id": "rqdv5juczeskfw1e867",
      "name": "Melting Pot",
      "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ...",
      "pictureId": "14",
      "city": "Medan",
      "rating": 4.2
    }),
    Restaurant.fromMap({
      "id": "rqdv5juczeskfw1e867",
      "name": "Melting Pot",
      "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ...",
      "pictureId": "14",
      "city": "Medan",
      "rating": 4.2
    }),
    Restaurant.fromMap({
      "id": "rqdv5juczeskfw1e867",
      "name": "Melting Pot",
      "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ...",
      "pictureId": "14",
      "city": "Medan",
      "rating": 4.2
    }),
    Restaurant.fromMap({
      "id": "rqdv5juczeskfw1e867",
      "name": "Melting Pot",
      "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ...",
      "pictureId": "14",
      "city": "Medan",
      "rating": 4.2
    }),
    Restaurant.fromMap({
      "id": "rqdv5juczeskfw1e867",
      "name": "Melting Pot",
      "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ...",
      "pictureId": "14",
      "city": "Medan",
      "rating": 4.2
    }),
    Restaurant.fromMap({
      "id": "rqdv5juczeskfw1e867",
      "name": "Melting Pot",
      "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ...",
      "pictureId": "14",
      "city": "Medan",
      "rating": 4.2
    }),
    Restaurant.fromMap({
      "id": "rqdv5juczeskfw1e867",
      "name": "Melting Pot",
      "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ...",
      "pictureId": "14",
      "city": "Medan",
      "rating": 4.2
    }),
    Restaurant.fromMap({
      "id": "rqdv5juczeskfw1e867",
      "name": "Melting Pot",
      "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ...",
      "pictureId": "14",
      "city": "Medan",
      "rating": 4.2
    }),
    Restaurant.fromMap({
      "id": "rqdv5juczeskfw1e867",
      "name": "Melting Pot",
      "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ...",
      "pictureId": "14",
      "city": "Medan",
      "rating": 4.2
    }),
    Restaurant.fromMap({
      "id": "rqdv5juczeskfw1e867",
      "name": "Melting Pot",
      "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ...",
      "pictureId": "14",
      "city": "Medan",
      "rating": 4.2
    }),
    Restaurant.fromMap({
      "id": "rqdv5juczeskfw1e867",
      "name": "Melting Pot",
      "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ...",
      "pictureId": "14",
      "city": "Medan",
      "rating": 4.2
    }),
    Restaurant.fromMap({
      "id": "rqdv5juczeskfw1e867",
      "name": "Melting Pot",
      "description": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. ...",
      "pictureId": "14",
      "city": "Medan",
      "rating": 4.2
    }),
  ];

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => restaurantBloc,
      child: Container(
        padding: const EdgeInsets.all(16),
        color: ColorConstant.lightPrimaryColor,
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    CupertinoIcons.search,
                    color: ColorConstant.darkPrimaryColor,
                    size: 24,
                  ),
                ),
              ),
              5.verticalSpace,
              BoldText(
                text: StringConstant.favorites,
                color: ColorConstant.darkPrimaryColor,
                size: 30.spMin,
              ),
              20.verticalSpace,
              Expanded(
                child: ListView.separated(
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    var item = listRestaurant[index];
                    return restaurantItem(item);
                  },
                  separatorBuilder: (context, index) {
                    return 10.verticalSpace;
                  },
                  itemCount: listRestaurant.length,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget restaurantItem(Restaurant restaurant) {
    return GestureDetector(
      onTap: () {},
      child: Row(
        children: [
          CachedNetworkImage(
            imageUrl: URLConstant.mediumImageUrl.replaceAll('<pictureId>', restaurant.pictureId),
            imageBuilder: (context, imageProvider) => Container(
              width: 100,
              height: 80,
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                  image: imageProvider,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          15.horizontalSpace,
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              BoldText(text: restaurant.name),
              5.verticalSpace,
              Row(
                children: [
                  const Icon(
                    CupertinoIcons.location_solid,
                    color: Colors.red,
                    size: 14,
                  ),
                  5.horizontalSpace,
                  MediumText(text: restaurant.city),
                ],
              ),
              5.verticalSpace,
              Row(
                children: [
                  const Icon(
                    CupertinoIcons.star_fill,
                    color: Colors.orange,
                    size: 14,
                  ),
                  5.horizontalSpace,
                  MediumText(text: restaurant.rating.toString()),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
