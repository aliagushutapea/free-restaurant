import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:free_restaurant/constant/asset_constant.dart';
import 'package:free_restaurant/constant/color_constant.dart';
import 'package:free_restaurant/constant/string_constant.dart';
import 'package:free_restaurant/feature/presentation/home_screen.dart';
import 'package:free_restaurant/widgets/text/semibold_text.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnimatedSplashScreen(
        splashTransition: SplashTransition.fadeTransition,
        splashIconSize: 1.sh,
        backgroundColor: ColorConstant.primaryColor,
        splash: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.asset(
                AssetConstant.iconLogo,
                width: 60.r,
                height: 70.r,
              ),
              3.verticalSpace,
              SemiboldText(
                text: StringConstant.versionApp,
                color: ColorConstant.primaryTextColor,
                size: 14.spMin,
              ),
            ],
          ),
        ),
        nextScreen: const HomeScreen(),
      ),
    );
  }
}
