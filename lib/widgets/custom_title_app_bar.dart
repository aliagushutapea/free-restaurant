import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:free_restaurant/constant/color_constant.dart';

class CustomTitleAppBar extends StatelessWidget {
  const CustomTitleAppBar({
    Key? key,
    required this.title,
    this.color,
  }) : super(key: key);

  final Color? color;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: 16.spMin,
        color: color ?? ColorConstant.primaryTextColor,
      ),
    );
  }
}
