import 'package:flutter/material.dart';
import 'package:free_restaurant/constant/color_constant.dart';

class ProgressBar extends StatelessWidget {
  const ProgressBar({
    Key? key,
    this.backgroundColor,
  }) : super(key: key);

  final Color? backgroundColor;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: size.width,
      color: backgroundColor ?? ColorConstant.blackColor.withOpacity(0.3),
      child: const Center(
        child: CircularProgressIndicator(
          color: ColorConstant.primaryColor,
        ),
      ),
    );
  }
}
