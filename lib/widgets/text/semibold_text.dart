import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:free_restaurant/constant/color_constant.dart';

class SemiboldText extends StatelessWidget {
  const SemiboldText({
    Key? key,
    required this.text,
    this.size,
    this.color,
    this.align,
    this.height,
    this.letterSpacing,
    this.maxLines,
    this.textOverflow,
    this.decoration,
    this.fontWeight,
  }) : super(key: key);

  final TextAlign? align;
  final Color? color;
  final double? height;
  final double? letterSpacing;
  final double? size;
  final String text;
  final int? maxLines;
  final FontWeight? fontWeight;
  final TextOverflow? textOverflow;
  final TextDecoration? decoration;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: align,
      maxLines: maxLines,
      overflow: textOverflow,
      style: TextStyle(
        fontFamily: 'Inter',
        fontSize: size ?? 16.spMin,
        height: height ?? 1.h,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w600,
        color: color ?? ColorConstant.primaryTextColor,
        decoration: decoration ?? TextDecoration.none,
      ),
    );
  }
}
