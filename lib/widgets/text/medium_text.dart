import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:free_restaurant/constant/color_constant.dart';

class MediumText extends StatelessWidget {
  const MediumText({
    Key? key,
    required this.text,
    this.size,
    this.color,
    this.align,
    this.softwrap,
    this.height,
    this.letterSpacing,
    this.maxLines,
    this.textOverflow,
    this.decoration,
    this.fontWeight,
    this.fontStyle,
    this.textWidthBasis,
  }) : super(key: key);

  final TextAlign? align;
  final Color? color;
  final double? height;
  final double? letterSpacing;
  final bool? softwrap;
  final double? size;
  final String text;
  final int? maxLines;
  final TextOverflow? textOverflow;
  final TextDecoration? decoration;
  final FontWeight? fontWeight;
  final FontStyle? fontStyle;
  final TextWidthBasis? textWidthBasis;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: align,
      maxLines: maxLines,
      overflow: textOverflow,
      textWidthBasis: textWidthBasis ?? TextWidthBasis.longestLine,
      style: TextStyle(
        fontFamily: 'Inter',
        fontSize: size ?? 14.spMin,
        height: height ?? 1.h,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w500,
        color: color ?? ColorConstant.primaryTextColor,
        decoration: decoration ?? TextDecoration.none,
        fontStyle: fontStyle,
      ),
    );
  }
}
