import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:free_restaurant/constant/color_constant.dart';

class RegularText extends StatelessWidget {
  const RegularText({
    Key? key,
    required this.text,
    this.size,
    this.color,
    this.align,
    this.height,
    this.letterSpacing,
    this.maxLines,
    this.textOverflow,
    this.decoration,
    this.softwrap,
    this.fontWeight,
    this.fontStyle,
    this.textWidthBasis,
  }) : super(key: key);

  final TextAlign? align;
  final Color? color;
  final FontWeight? fontWeight;
  final double? height;
  final double? letterSpacing;
  final double? size;
  final String text;
  final bool? softwrap;
  final int? maxLines;
  final TextOverflow? textOverflow;
  final TextDecoration? decoration;
  final FontStyle? fontStyle;
  final TextWidthBasis? textWidthBasis;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: align,
      softWrap: softwrap,
      maxLines: maxLines,
      overflow: textOverflow,
      textWidthBasis: textWidthBasis ?? TextWidthBasis.longestLine,
      style: TextStyle(
        fontFamily: 'Inter',
        fontSize: size ?? 12.spMin,
        height: height ?? 1.h,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w400,
        color: color ?? ColorConstant.primaryTextColor,
        decoration: decoration ?? TextDecoration.none,
        fontStyle: fontStyle,
      ),
    );
  }
}
