import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:free_restaurant/constant/color_constant.dart';

class BoldText extends StatelessWidget {
  const BoldText({
    Key? key,
    required this.text,
    this.size,
    this.color,
    this.align,
    this.height,
    this.letterSpacing,
    this.maxLines,
    this.textOverflow,
    this.decoration,
    this.textWidthBasis,
  }) : super(key: key);

  final TextAlign? align;
  final Color? color;
  final double? height;
  final double? letterSpacing;
  final double? size;
  final String text;
  final int? maxLines;
  final TextOverflow? textOverflow;
  final TextDecoration? decoration;
  final TextWidthBasis? textWidthBasis;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: align,
      maxLines: maxLines,
      overflow: textOverflow,
      textWidthBasis: textWidthBasis ?? TextWidthBasis.longestLine,
      style: TextStyle(
        fontFamily: 'Inter',
        fontSize: size ?? 18.spMin,
        height: height ?? 1.h,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w700,
        color: color ?? ColorConstant.primaryTextColor,
        decoration: decoration ?? TextDecoration.none,
      ),
    );
  }
}
