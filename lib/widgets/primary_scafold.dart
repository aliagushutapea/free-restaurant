import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:free_restaurant/constant/color_constant.dart';
import 'package:free_restaurant/widgets/custom_title_app_bar.dart';

class PrimaryScaffold extends StatelessWidget {
  const PrimaryScaffold({
    Key? key,
    required this.childContainer,
    this.titleBar,
    this.paddingContainer,
    this.colorContainer,
    this.floatingButton,
    this.floatingLocation,
    this.actions,
    this.centerTitle = true,
    this.resizeToAvoidBottomInset,
  }) : super(key: key);

  final Widget childContainer;
  final Color? colorContainer;
  final bool centerTitle;
  final EdgeInsetsGeometry? paddingContainer;
  final String? titleBar;
  final Widget? floatingButton;
  final FloatingActionButtonLocation? floatingLocation;
  final List<Widget>? actions;
  final bool? resizeToAvoidBottomInset;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: resizeToAvoidBottomInset,
      appBar: AppBar(
        iconTheme: const IconThemeData(color: ColorConstant.whiteColor),
        centerTitle: centerTitle,
        title: CustomTitleAppBar(title: titleBar ?? ''),
        titleSpacing: 2.spMin,
        backgroundColor: ColorConstant.whiteColor,
        elevation: 0.0,
        actions: actions,
      ),
      body: Container(
        width: size.width,
        color: colorContainer ?? Colors.white,
        child: Stack(
          children: [
            Container(
              padding: paddingContainer ??
                  const EdgeInsets.only(
                    left: 16,
                    right: 16,
                    bottom: 16,
                  ).r,
              child: childContainer,
            ),
          ],
        ),
      ),
      floatingActionButton: floatingButton,
      floatingActionButtonLocation: floatingLocation,
    );
  }
}
