import 'package:dio/dio.dart';
import 'package:free_restaurant/constant/url_constant.dart';
import 'package:free_restaurant/data/model/restaurant.dart';

class RestaurantRepository {
  final Dio dioInterceptor;
  RestaurantRepository({required this.dioInterceptor});

  Future<List<Restaurant>> getAllRestaurant() async {
    var urlGetAllRestaurant = URLConstant.pathListRestaurant;
    var response = await dioInterceptor.get(urlGetAllRestaurant);
    var responseData = response.data;
    List bodyRestaurants = responseData['restaurants'] ?? [];
    var listRestaurant = bodyRestaurants.map((e) => Restaurant.fromMap(e)).toList();
    return listRestaurant;
  }
}
